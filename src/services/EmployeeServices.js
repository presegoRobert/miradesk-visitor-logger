import { ref } from 'vue'
import axios from '../util/api';
import { useRouter } from 'vue-router';

export default function employeeServices() {
    const router = useRouter()
    const employees = ref({});
    const comboEmployee = ref({});
    const employee = ref('');
    const errors = ref('')

    const getEmployees = async (page) => {
        const response = await axios.get('/employees/page/' + page)
        employees.value = response.data;
    }

    const getComboEmployee = async () => {
        const response = await axios.get('/employees')
        comboEmployee.value = response.data;
    }

    const getAnEmployee = async () => {
        const response = await axios.get('/employees/' + router.currentRoute._value.params.id);
        employee.value = response.data.data;
    }

    const storeEmployee = async (data) => {
        errors.value = ''
        try {
            await axios.post('/employees', data)
            await router.push('/admin/employee');
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const updateEmployee = async (updata) => {
        let data = updata.employee._value;
        errors.value = ''
        try {
            await axios.put('/employees/' + updata.employee._value.id, data)
            await router.push('/admin/employee');
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const deleteEmployee = async (id) => {
        errors.value = ''
        try {
            await axios.delete('/employees/'+id);
            getEmployees(1);
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    return {
        employees,
        getEmployees,
        comboEmployee,
        getComboEmployee,
        employee,
        getAnEmployee,
        storeEmployee,
        updateEmployee,
        deleteEmployee,
    }
}