import { ref } from 'vue'
import axios from '../util/api';

export default function AdminServices() {
    const visitors = ref([])

    const getVisitors = async () => {
        const response = await axios.get('/admin/visitors');
        visitors.value = response.data.data;
    }

    const destroyVisitor = async (id) => {
        await axios.put('/visitors/' + id);
        getVisitors();
    }

    return {
        visitors,
        getVisitors,
        destroyVisitor,
    }
}
