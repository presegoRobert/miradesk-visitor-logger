import axios from '../util/api';

export default function settingServices() {
    const fileUpload = async (fromdata) => {
        await axios.post('/settings/background-upload',fromdata);
    }

    return {
        fileUpload,
    }
}