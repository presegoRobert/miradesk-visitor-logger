import { ref } from 'vue'
import axios from '../util/api';
import { useRouter } from 'vue-router';

export default function departmentServices() {
    const router = useRouter()
    const departments = ref({});
    const comboDepartment = ref({});
    const department = ref('');
    const errors = ref('')

    const getDepartments = async (page) => {
        const response = await axios.get('/departments/page/' + page)
        departments.value = response.data;
    }

    const getComboDepartment = async () => {
        const response = await axios.get('/departments')
        comboDepartment.value = response.data;
    }

    const getAnDepartment = async () => {
        const response = await axios.get('/departments/' + router.currentRoute._value.params.id);
        department.value = response.data.data;
    }

    const storeDepartment = async (data) => {
        errors.value = ''
        try {
            await axios.post('/departments', data)
            await router.push('/admin/department');
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const updateDepartment = async (update) => {
        let data = update.department._value;
        errors.value = ''
        try {
            await axios.put('/departments/' + update.department._value.id, data)
            await router.push('/admin/department');
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    const deleteDepartment = async (id) => {
        errors.value = ''
        try {
            await axios.delete('/departments/'+id);
            getDepartments(1);
        } catch (e) {
            if (e.response.status === 422) {
                errors.value = e.response.data.errors
            }
        }
    }

    return {
        departments,
        getDepartments,
        comboDepartment,
        getComboDepartment,
        department,
        getAnDepartment,
        storeDepartment,
        updateDepartment,
        deleteDepartment,
    }
}
