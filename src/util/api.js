import axios from 'axios';

export const baseURL = `${process.env.VUE_APP_BACKEND_URL}api`;

export default axios.create({
  baseURL: baseURL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});
